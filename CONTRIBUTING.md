# Contributing guidelines

## Filing issues

If you have a question or have a problem using genomic-tool, please create an [issue](https://gitlab.com/uit-sfb/genomic-tools/issues).


## Submitting patches (pull requests)

We'd love to accept your patches!


## Technical details

To learn more about implementation details and guidelines, please visit our [Wiki](https://gitlab.com/uit-sfb/genomic-tools/wikis/Contributing).